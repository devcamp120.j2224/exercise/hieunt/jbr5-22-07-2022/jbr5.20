package com.jbr520.artistalbumapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.*;

import com.jbr520.artistalbumapi.model.Album;
import com.jbr520.artistalbumapi.service.AlbumService;


@RestController
@CrossOrigin
public class AlbumController {
    @GetMapping("/album-info")
    public Album getAlbumById(@RequestParam(required = true) int albumId) {
        ArrayList<Album> allAlbums = AlbumService.getAllAlbums();
        Album result = null;
        for (Album bAlbum : allAlbums) {  
            if(bAlbum.getId() == albumId) {
                result = bAlbum;
            }
        }
        return result;
    }
}
