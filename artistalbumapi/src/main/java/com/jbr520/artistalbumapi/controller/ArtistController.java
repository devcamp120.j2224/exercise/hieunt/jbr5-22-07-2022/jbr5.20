package com.jbr520.artistalbumapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.*;

import com.jbr520.artistalbumapi.model.Artist;
import com.jbr520.artistalbumapi.service.ArtistService;


@RestController
@CrossOrigin
public class ArtistController {
    @GetMapping("/artists")
    public ArrayList<Artist> getArtistList(){
        ArrayList<Artist> artistList = ArtistService.getAllArtists();
        return artistList;
    }

    @GetMapping("/artist-info")
    public Artist getArtistById(@RequestParam(required = true) int artistId) {
        ArrayList<Artist> artistList = ArtistService.getAllArtists();
        Artist result = null;
        for(Artist bArtist : artistList) {
           if(bArtist.getId() == artistId) {
                result = bArtist;
            }
        }
        return result;
    }
}
