package com.jbr520.artistalbumapi.model;

import java.util.ArrayList;

public class Album {
    private int id;
    private String name;
    private ArrayList<String> songs;   
    public Album() {
    }
    public Album(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public Album(int id, String name, ArrayList<String> songs) {
        this(id, name);
        this.songs = songs;
    }
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public ArrayList<String> getSongs() {
        return songs;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setSongs(ArrayList<String> songs) {
        this.songs = songs;
    }
}
