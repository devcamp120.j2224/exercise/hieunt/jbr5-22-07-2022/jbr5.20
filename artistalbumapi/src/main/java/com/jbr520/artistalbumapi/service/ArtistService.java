package com.jbr520.artistalbumapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.jbr520.artistalbumapi.model.Artist;


@Service
public class ArtistService {
    private static ArrayList<Artist> allArtists = new ArrayList<Artist>();
    static {
        Artist artist1 = new Artist(1, "Lan",
            AlbumService.getAlbumList1());
        Artist artist2 = new Artist(2, "Hoa",
            AlbumService.getAlbumList2());
        Artist artist3 = new Artist(3, "Nam",
            AlbumService.getAlbumList3());
        allArtists.add(artist1);
        allArtists.add(artist2);
        allArtists.add(artist3);
    }
    public static ArrayList<Artist> getAllArtists() {
        return allArtists;
    }
    public static void setAllArtists(ArrayList<Artist> allArtists) {
        ArtistService.allArtists = allArtists;
    }
}
