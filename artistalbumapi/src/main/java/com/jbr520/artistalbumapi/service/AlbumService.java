package com.jbr520.artistalbumapi.service;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.stereotype.Service;

import com.jbr520.artistalbumapi.model.Album;

@Service
public class AlbumService {
    private static ArrayList<Album> albumList1 = new ArrayList<Album>();
    private static ArrayList<Album> albumList2 = new ArrayList<Album>();
    private static ArrayList<Album> albumList3 = new ArrayList<Album>();
    private static ArrayList<Album> allAlbums = new ArrayList<Album>();
    static {
        Album album11 = new Album(11, "A1",
            new ArrayList<String>(Arrays.asList("A11", "A12", "A13")));
        Album album12 = new Album(12, "A2",
            new ArrayList<String>(Arrays.asList("A21", "A22", "A23")));
        Album album13 = new Album(13, "A3",
            new ArrayList<String>(Arrays.asList("A31", "A32", "A33")));
        Album album21 = new Album(21, "B1",
            new ArrayList<String>(Arrays.asList("B11", "B12", "B13")));
        Album album22 = new Album(22, "B2",
            new ArrayList<String>(Arrays.asList("B21", "B22", "B23" )));
        Album album23 = new Album(23, "B3",
            new ArrayList<String>(Arrays.asList("B31", "B32", "B33")));
        Album album31 = new Album(31, "C1",
            new ArrayList<String>(Arrays.asList("C11", "C12", "C13")));
        Album album32 = new Album(32, "C2",
            new ArrayList<String>(Arrays.asList("C21", "C22", "C23")));
        Album album33 = new Album(33, "C3",
            new ArrayList<String>(Arrays.asList("C31", "C32", "C33")));
        albumList1.add(album11); albumList1.add(album12); albumList1.add(album13);
        albumList2.add(album21); albumList2.add(album22); albumList2.add(album23);
        albumList3.add(album31); albumList3.add(album32); albumList3.add(album33);
        for (Album bAlbum : albumList1) {
            allAlbums.add(bAlbum);
        }
        for (Album bAlbum : albumList2) {
            allAlbums.add(bAlbum);
        }
        for (Album bAlbum : albumList3) {
            allAlbums.add(bAlbum);
        }
    }
    public static ArrayList<Album> getAlbumList1() {
        return albumList1;
    }
    public static void setAlbumList1(ArrayList<Album> albumList1) {
        AlbumService.albumList1 = albumList1;
    }
    public static ArrayList<Album> getAlbumList2() {
        return albumList2;
    }
    public static void setAlbumList2(ArrayList<Album> albumList2) {
        AlbumService.albumList2 = albumList2;
    }
    public static ArrayList<Album> getAlbumList3() {
        return albumList3;
    }
    public static void setAlbumList3(ArrayList<Album> albumList3) {
        AlbumService.albumList3 = albumList3;
    }
    public static ArrayList<Album> getAllAlbums() {
        return allAlbums;
    }
}
